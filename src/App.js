import React from 'react';
import './App.css';
import PokeStore from "./pages/PokeStore";

function App() {
    return (
        <PokeStore/>
    );
}

export default App;
