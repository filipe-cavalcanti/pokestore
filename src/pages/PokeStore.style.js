import makeStyles from "@material-ui/core/styles/makeStyles";
import {createStyles} from "@material-ui/core";

const useStyle = makeStyles(theme =>
    createStyles({
        mT5: {
            marginTop: '30px'
        },
        pokeStoreGrid: {
            [theme.breakpoints.up('lg')]: {
                display: 'grid',
                gridTemplateColumns: '5fr 2fr'
            },
            [theme.breakpoints.down('xs')]: {
                display: 'flex',
                flexDirection: 'column-reverse',
            },
        },
    })
)

export default useStyle;