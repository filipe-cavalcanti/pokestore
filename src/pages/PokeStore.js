import React, {useEffect, useState} from 'react';
import ToolBar from "../components/ToolBar/ToolBar";
import PokemonContainer from "../containers/PokemonContainer/PokemonContainer";
import useStyle from "./PokeStore.style";
import CartContainer from "../containers/CartContainer/CartContainer";

function PokeStore() {
    const style = useStyle();
    const [cart, setCart] = useState([])
    const [total, setTotal] = useState(0);
    useEffect(() => {
        const cartLocalStorage = JSON.parse(localStorage.getItem('cart'));
        if (cartLocalStorage) {
            setCart(cartLocalStorage);
            calculateTotal(cartLocalStorage);
        }
    },[])
    const addPokemon = (pokemon) => {
        setCart([...cart, pokemon]);
        localStorage.setItem('cart', JSON.stringify(cart));
        calculateTotal([...cart, pokemon]);
    };
    const calculateTotal = pokemon => {
        setTotal(pokemon
            .map(pokemon => pokemon.weight)
            .reduce((total, valorAtual) => total + valorAtual, 0));
    };
    return (
        <>
            <ToolBar/>
            <section className={style.pokeStoreGrid}>
                <section className={style.mT5}>
                    <PokemonContainer addPokemon={addPokemon}/>
                </section>
                <section className={style.cart}>
                    <CartContainer carts={cart} total={total} onBuy={() => {
                        setCart([]);
                        calculateTotal([]);
                    }}/>
                </section>
            </section>
        </>
    );
}

export default PokeStore;