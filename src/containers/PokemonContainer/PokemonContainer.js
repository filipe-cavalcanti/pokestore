import React, {useEffect, useState} from 'react';
import PokemonCard from "../../components/PokemonCard/PokemonCard";
import useStyle from "./PokemonContainer.style";
import Container from "@material-ui/core/Container";
import api from '../../api';
import Pagination from "@material-ui/lab/Pagination";

function PokemonContainer({addPokemon}) {
    const style = useStyle();
    const pageLength = 40;
    const [pokemon, setPokemon] = useState([]);
    const [totalPages, setTotalPages] = useState(0);
    const [actualPage, setActualPage] = useState(0);
    const handleChangePage = (event, page) => {
        setActualPage(page - 1);
        api.get(`?limit=${pageLength}&offset=${(page - 1)*pageLength}`).then(result => {
            setPokemon(result.data.results);
            calculateTotalPages(result.data.count);
        });
    };
    const calculateTotalPages = totalElements => totalElements % pageLength === 0 ?
        setTotalPages(parseInt((totalElements / pageLength).toFixed())) :
        setTotalPages(parseInt((totalElements / pageLength).toFixed()) + 1)
    useEffect(() => {
        api.get(`?limit=${pageLength}&offset=${actualPage*pageLength}`).then(result => {
            setPokemon(result.data.results);
            calculateTotalPages(result.data.count);
        });
    }, [actualPage])
    return (
        <>
            <Container maxWidth='lg'>
                <div className={style.pokemonContainer}>
                    {pokemon.map((item, i) => <PokemonCard key={i}
                                                           name={item.name}
                                                           url={item.url}
                                                           action={addPokemon}/>)}
                </div>
                <Pagination count={totalPages} color="primary" onChange={handleChangePage}/>
            </Container>
        </>
    );
}

export default PokemonContainer;