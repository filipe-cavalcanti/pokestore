import makeStyles from "@material-ui/core/styles/makeStyles";
import {createStyles} from "@material-ui/core";

const useStyle = makeStyles(() =>
    createStyles({
        pokemonContainer: {
            display: 'flex',
            flexWrap: 'wrap',
        }
    })
)

export default useStyle;