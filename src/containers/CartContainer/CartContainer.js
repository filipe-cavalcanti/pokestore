import React, {useState} from 'react';
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import useStyle from "./CartContainer.style";
import Button from "@material-ui/core/Button";
import BuyDialog from "../../components/buyDialog/buyDialog"
function CartContainer({carts, total, onBuy}) {
    const style = useStyle();
    const [openBuyDialog, setOpenBuyDialog] = useState(false)
    return (
        <Card className={[style.mR5, style.mT5, style.card]}>
            <Typography variant='h4' align='center'>Carrinho</Typography>
            <TableContainer>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Pokemon</TableCell>
                            <TableCell align='right'>Preço</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {carts && carts.map((pokemon, i) => <TableRow key={i}>
                            <TableCell className={style.flexCenter}>
                                <img src={pokemon.sprites.front_default} alt='Pokemon'/>
                                {pokemon.name}
                            </TableCell>
                            <TableCell align='right'>R$ {pokemon.weight}</TableCell>
                        </TableRow>)}
                    </TableBody>
                </Table>
            </TableContainer>
            <Typography variant='h6' align='center'>Total R$ ${total}</Typography>
            <Button variant="contained" color='primary' fullWidth size='large' onClick={() => setOpenBuyDialog(!openBuyDialog)}>Finalizar Compra</Button>
            <BuyDialog open={openBuyDialog} onClose={() => {
                setOpenBuyDialog();
                onBuy();
            }}/>
        </Card>
    );
}

export default CartContainer;