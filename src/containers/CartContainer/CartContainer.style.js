import makeStyles from "@material-ui/core/styles/makeStyles";
import {createStyles} from "@material-ui/core";

const useStyle = makeStyles(theme =>
    createStyles({
        mT5: {
            marginTop: '30px',
        },
        mR5: {
            marginRight: '30px'
        },
        card: {
            maxHeight: '500px',
            overflowY: 'scroll'
        },
        flexCenter: {
            display: 'flex',
            alignItems: 'center'
        },
        [theme.breakpoints.down('xs')]: {
            card: {
                maxWidth: '100%',
                maxHeight: '250px',
                flexGrow: 1,
                marginLeft: '30px'
            }
        },
    }
))

export default useStyle;