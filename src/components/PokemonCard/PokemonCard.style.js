import makeStyles from "@material-ui/core/styles/makeStyles";
import {createStyles} from "@material-ui/core";

const useStyle = makeStyles((theme) =>
    createStyles({
        card: {
            width: '166px',
            height: '250px',
            marginLeft: '10px',
            marginBottom: '10px',
        },
        title: {
            fontSize: '100px',
            margin: '0'
        },
        pokeImage: {
            width: '100%',
            height: '150px'
        },
        text: {
            margin: '0',
            padding: '0'
        },
        [theme.breakpoints.down('xs')]: {
            card: {
                maxWidth: '100%',
                maxHeight: '250px',
                flexGrow: 1
            }
        }
    }
))

export default useStyle;