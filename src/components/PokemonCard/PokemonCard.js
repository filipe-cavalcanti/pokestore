import React, {useEffect, useState} from 'react';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import useStyle from "./PokemonCard.style";
import api from "../../api";
import Typography from "@material-ui/core/Typography";

function PokemonCard({name, url, action}) {
    const style = useStyle();
    const [pokemon, setPokemon] = useState({weight: 0});
    const [pokeImage, setPokeImage] = useState();
    useEffect(() => {
        api.get(url).then(result => {
            setPokemon(result.data);
            setPokeImage(<img className={style.pokeImage} src={`https://pokeres.bastionbot.org/images/pokemon/${result.data.id}.png`} alt='Pokemon'/>);
        })
    }, [style.pokeImage, url])
    return (
        <Card className={style.card} elevation={3} onClick={() => action(pokemon)}>
            <CardContent>
                {pokeImage}
                <div>
                    <Typography variant='subtitle1' className={style.text}>{name}</Typography>
                    <Typography variant='subtitle1' className={style.text}> R$ {pokemon.weight}</Typography>
                </div>
            </CardContent>
        </Card>
    );
}

export default PokemonCard;